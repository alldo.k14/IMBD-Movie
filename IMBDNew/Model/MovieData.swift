//
//  MovieData.swift
//  IMBDNew
//
//  Created by Alldo Kurniawan.
//

import Foundation

struct MovieData {
}

//MARK: - Genre Section
struct ListGenreData: Codable {
    let genres: [Genres]
}

struct Genres: Codable {
    let id: Int
    let name: String
}

//MARK: - Movie Section
struct ListMovieData: Codable {
    let results: [Movies]
}

struct Movies: Codable {
    let id: Int
    let title: String
    let vote_average: Double
}

//MARK: - Detail Movie Section
struct DetailMovieData: Codable {
    let id: Int
    let title: String
    let overview: String
    let release_date: String
    let runtime: Int
}

//MARK: - Review Section
struct Reviews: Codable {
    let results: [DetailReviews]
}

struct DetailReviews: Codable {
    let author: String
    let content: String
}
