//
//  MovieModel.swift
//  IMBDNew
//
//  Created by Alldo Kurniawan.
//

import Foundation

struct MovieModel {
}

//MARK: - Genre Model
struct ListGenreModel {
    let genreName: String
    let genreId: Int
}

//MARK: - Movie Model
struct ListMovieModel {
    let movieId: Int
    let movieName: String
    let rating: Double
}

//MARK: - Detail Movie Model
struct DetailMovieModel {
    let movieId: Int
    let movieName: String
    let movieDesc: String
    let movieDuration: Int
    let movieReleaseDate: String
}

//MARK: - Review Movie Model
struct ReviewModel {
    let author: String
    let comments: String
}
