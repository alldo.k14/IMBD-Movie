//
//  DetailMovieViewController.swift
//  IMBDNew
//
//  Created by Alldo Kurniawan.
//

import UIKit

class DetailMovieViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var reviewTableView: UITableView!
    
    var movieManager = MovieManager()
    var listReview: [ReviewModel] = []
    
    var id: Int = 0
    var name: String = ""
    
    var tempPage = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = name
        
        reviewTableView.dataSource = self
        reviewTableView.delegate = self
        movieManager.delegate = self
        
        movieManager.fetchData(code: id, identifier: "detailMovie", page: nil)
        movieManager.fetchData(code: id, identifier: "review", page: tempPage)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        tempPage = 1
        listReview.removeAll()
    }
    
    

}

extension DetailMovieViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listReview.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = reviewTableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath)
        cell.textLabel?.text = listReview[indexPath.row].author
        cell.detailTextLabel?.text = listReview[indexPath.row].comments
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !listReview.isEmpty {
            if indexPath.row == listReview.count - 1 {
                movieManager.fetchData(code: id, identifier: "review", page: tempPage)
            }
        }
    }
}

extension DetailMovieViewController: MovieManagerDelegate {
    func updateReview(listOfReview: [ReviewModel]) {
        DispatchQueue.main.async {
            self.listReview.append(contentsOf: listOfReview)
            self.reviewTableView.reloadData()
        }
        tempPage += 1
    }
    
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String) {
        DispatchQueue.main.async {
            self.titleLabel.text = name
            self.descLabel.text = desc
            self.durationLabel.text = "Duration: \(duration) mins"
            self.releaseDateLabel.text = releaseDate
        }
    }
    
    func updateGenre(listOfGenre: [ListGenreModel]) {
    }
    
    func updateMovie(listOfMovie: [ListMovieModel]) {
    }
    
    
}
