//
//  ViewController.swift
//  IMBDNew
//
//  Created by Alldo Kurniawan.
//

import UIKit

class GenreViewController: UIViewController {

    @IBOutlet weak var genreTableView: UITableView!
    
    var movieManager = MovieManager()
    var listGenre: [ListGenreModel] = []
    
    var chosenName: String = ""
    var chosenId: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieManager.delegate = self
        movieManager.fetchData(code: 0, identifier: "genre", page: nil)
        genreTableView.dataSource = self
        genreTableView.delegate = self
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChosenGenre" {
            let destinationVC = segue.destination as! ListMovieViewController
            destinationVC.id = chosenId
            destinationVC.name = chosenName
        }
    }
    
}

extension GenreViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listGenre.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = genreTableView.dequeueReusableCell(withIdentifier: "genreCell", for: indexPath)
        cell.textLabel?.text = listGenre[indexPath.row].genreName
        cell.tag = listGenre[indexPath.row].genreId
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenName = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!
        chosenId = tableView.cellForRow(at: indexPath)!.tag
        self.performSegue(withIdentifier: "goToChosenGenre", sender: self)
    }
    
}

extension GenreViewController: MovieManagerDelegate {
    func updateReview(listOfReview: [ReviewModel]) {
    }
    
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String) {
    }
    
    func updateMovie(listOfMovie: [ListMovieModel]) {
    }
    
    func updateGenre(listOfGenre: [ListGenreModel]) {
        DispatchQueue.main.async {
            self.listGenre = listOfGenre
            self.genreTableView.reloadData()
        }
    }
}

