//
//  ChosenMovieViewController.swift
//  IMBDNew
//
//  Created by Alldo Kurniawan.
//

import UIKit

class ListMovieViewController: UIViewController {
    
    @IBOutlet weak var listMovieTableView: UITableView!
    
    var movieManager = MovieManager()
    var listMovie: [ListMovieModel] = []
    
    var id: Int = 0
    var name: String = ""
    
    var chosenName: String = ""
    var chosenId: Int = 0
    
    var tempPage = 1
    
    var fetchMore = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = name
        
        movieManager.delegate = self
        movieManager.fetchData(code: id, identifier: "chosenMovie", page: tempPage)
        listMovieTableView.dataSource = self
        listMovieTableView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToChosenMovie" {
            let destinationVC = segue.destination as! DetailMovieViewController
            destinationVC.id = chosenId
            destinationVC.name = chosenName
        }
    }

}

extension ListMovieViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listMovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = listMovieTableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath)
        cell.textLabel?.text = listMovie[indexPath.row].movieName
        cell.detailTextLabel?.text = "Rating: \(listMovie[indexPath.row].rating)"
        cell.tag = listMovie[indexPath.row].movieId
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chosenName = (tableView.cellForRow(at: indexPath)?.textLabel?.text)!
        chosenId = tableView.cellForRow(at: indexPath)!.tag
        self.performSegue(withIdentifier: "goToChosenMovie", sender: self)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !listMovie.isEmpty {
            if indexPath.row == listMovie.count - 1 {
                movieManager.fetchData(code: id, identifier: "chosenMovie", page: tempPage)
            }
        }
    }
    
}

extension ListMovieViewController: MovieManagerDelegate {
    func updateReview(listOfReview: [ReviewModel]) {
    }
    
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String) {
    }
    
    func updateMovie(listOfMovie: [ListMovieModel]) {
        DispatchQueue.main.async {
            self.listMovie.append(contentsOf: listOfMovie)
            self.listMovieTableView.reloadData()
        }
        tempPage += 1
    }
    
    func updateGenre(listOfGenre: [ListGenreModel]) {
    }
    
    
}
