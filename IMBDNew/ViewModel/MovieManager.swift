//
//  MovieManager.swift
//  IMBDNew
//
//  Created by Alldo Kurniawan.
//

import Foundation

protocol MovieManagerDelegate {
    func updateGenre(listOfGenre: [ListGenreModel])
    func updateMovie(listOfMovie: [ListMovieModel])
    func updateDetaiMovie(id: Int, name: String, desc: String, duration: Int, releaseDate: String)
    func updateReview(listOfReview: [ReviewModel])
}

class MovieManager {
    
    var delegate: MovieManagerDelegate?
    
    //MARK: - Defining URL
    let genreURL = "https://api.themoviedb.org/3/genre/movie/list?api_key=04333d9ac968c5e50b5b5e8f4b5e28b8&language=en-US"
    let listMovieURL = "https://api.themoviedb.org/3/discover/movie?api_key=04333d9ac968c5e50b5b5e8f4b5e28b8&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page="
    let listMovieURL2 = "&with_watch_monetization_types=flatrate&with_genres="
    let detailMovieURL = "https://api.themoviedb.org/3/movie"
    
    //MARK: - Fetch Data
    func fetchData(code: Int, identifier: String, page: Int?){
        if identifier == "genre" {
            performRequest(with: genreURL, id: identifier)
        } else if identifier == "chosenMovie" {
            if let page = page {
                let stringURL = "\(listMovieURL)\(page)\(listMovieURL2)\(code)"
                performRequest(with: stringURL, id: identifier)
            }
        } else if identifier == "detailMovie" {
            let stringURL = "\(detailMovieURL)/\(code)?api_key=04333d9ac968c5e50b5b5e8f4b5e28b8&language=en-US"
            performRequest(with: stringURL, id: identifier)
        } else if identifier == "review" {
            if let page = page {
                let stringURL = "\(detailMovieURL)/\(code)/reviews?api_key=04333d9ac968c5e50b5b5e8f4b5e28b8&language=en-US&page=\(page)"
                performRequest(with: stringURL, id: identifier)
            }
        }
    }
    
    //MARK: - Perform Request
    func performRequest(with urlString: String, id identifier: String){
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { data, response, error in
                if error != nil {
                    //error
                    return
                }
                if let safeData = data {
                    if identifier == "genre" {
                        if let genres = self.parseJSONGenre(safeData) {
                            self.delegate?.updateGenre(listOfGenre: genres)
                        }
                    } else if identifier == "chosenMovie" {
                        if let movies = self.parseJSONListMovie(safeData) {
                            self.delegate?.updateMovie(listOfMovie: movies)
                        }
                    } else if identifier == "detailMovie" {
                        if let movies = self.parseJSONDetailMovie(safeData) {
                            self.delegate?.updateDetaiMovie(id: movies.movieId, name: movies.movieName, desc: movies.movieDesc, duration: movies.movieDuration, releaseDate: movies.movieReleaseDate)
                        }
                    } else if identifier == "review" {
                        if let reviews = self.parseJSONListReview(safeData) {
                            self.delegate?.updateReview(listOfReview: reviews)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    //MARK: - Parse JSON
    
    //MARK: Genre
    func parseJSONGenre(_ genreData: Data) -> [ListGenreModel]? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(ListGenreData.self, from: genreData)
            let genres = decodedData.genres
            var fetchedGenre: [ListGenreModel] = []
            
            for eachGenre in genres {
                let name = eachGenre.name
                let id = eachGenre.id
                
                fetchedGenre.append(ListGenreModel(genreName: name, genreId: id))
            }
            
            return fetchedGenre
        } catch {
            //error
            return nil
        }
    }
    
    //MARK: List Movie
    func parseJSONListMovie(_ movieData: Data) -> [ListMovieModel]? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(ListMovieData.self, from: movieData)
            let movies = decodedData.results
            var fetchedMovies: [ListMovieModel] = []
            
            for eachMovie in movies {
                let name = eachMovie.title
                let id = eachMovie.id
                let rate = eachMovie.vote_average
                
                fetchedMovies.append(ListMovieModel(movieId: id, movieName: name, rating: rate))
            }
            
            return fetchedMovies
        } catch {
            //belum selesai
            return nil
        }
    }
    
    //MARK: Detail Movie
    func parseJSONDetailMovie(_ movieDetailData: Data) -> DetailMovieModel? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(DetailMovieData.self, from: movieDetailData)
            let id = decodedData.id
            let name = decodedData.title
            let desc = decodedData.overview
            let duration = decodedData.runtime
            let releaseDate = decodedData.release_date
            
            let fetchedDetailMovie = DetailMovieModel(movieId: id, movieName: name, movieDesc: desc, movieDuration: duration, movieReleaseDate: releaseDate)
            
            return fetchedDetailMovie
        } catch {
            //error
            return nil
        }
    }
    
    //MARK: Movie Review
    func parseJSONListReview(_ reviewData: Data) -> [ReviewModel]? {
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode(Reviews.self, from: reviewData)
            let reviews = decodedData.results
            var fetchedReviews: [ReviewModel] = []
            
            for eachReview in reviews {
                let author = eachReview.author
                let comments = eachReview.content
                
                fetchedReviews.append(ReviewModel(author: author, comments: comments))
            }
            
            return fetchedReviews
        } catch {
            //error
            return nil
        }
    }
    
}
